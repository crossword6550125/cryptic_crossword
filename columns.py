def get_columns(rows: list) -> list:
    length = len(rows[0])
    return [''.join([str[i] for str in rows]) for i in range(length)]


print(get_columns(["________#____","_#_#_#_#_#_#_","_____#_______","_#_#_#_#_#_#_","#____________","_#_#_#_#_###_","______#______","_###_#_#_#_#_","____________#","_#_#_#_#_#_#_","_______#_____","_#_#_#_#_#_#_","____#________"]))
