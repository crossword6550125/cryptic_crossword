# cross = '''
# WWWWW
# WWBWW
# BWWWB
# WWWWB
# BBBWW
# '''

# cross = "_______#_______\n_#_#######\n_#\n#######\n####\n_########\n_#\n#######\n#\n########\n####_"

cross = """
________#______
_#_#_#_###_#_#_
________#______
_#_#_#_#_#_#_#_
___________####
_#_###_#_#_#_#_
_____#_________
_#_#_#_#_#_#_#_
_________#_____
_#_#_#_#_###_#_
####___________"""

BLACK, WHITE = "#", "_"
# BLACK, WHITE ='B', 'W'

grid = cross.strip('\n').split('\n')
print(grid)
rows = len(grid[0])

def block(row: str) -> list:
    r = 0
    ans = []
    for i in range (len(row) - 2):
        if row[i] + row [i + 1] + row[i + 2] == BLACK + WHITE + WHITE:
            r = i + 1
            ans.append(r)
    b = row.index(BLACK)
    if '__' in row[:b]:
        ans.append(0)
    return ans
            
for i in range (len(grid)):            
    print(block(grid[i]))
    
