BLACK, WHITE = "#", "_"
# BLACK, WHITE ='B', 'W'

cross = """
________#______
_#_#_#_###_#_#_
________#______
_#_#_#_#_#_#_#_
___________####
_#_###_#_#_#_#_
_____#_________
_#_#_#_#_#_#_#_
_________#_____
_#_#_#_#_###_#_
####___________
_#_#_#_#_#_#_#_
______#________
_#_#_###_#_#_#_
______#________"""

def get_columns(rows: list) -> list:
    length = len(rows[0])
    return [''.join([row[i] for row in rows]) for i in range(length)]

def rows(CROSSWORD: str) -> list[str]:
    return CROSSWORD.split('\n')

# print(get_columns(rows(cross.strip('\n'))))


def block(row: str) -> list:
    r = 0
    ans = []
    #print(row)
    for i in range (len(row) - 2):
        if row[i] + row [i + 1] + row[i + 2] == BLACK + WHITE + WHITE:
            r = i + 1   
            ans.append(r)
    b = row.find(BLACK)
    if (b > -1 and '__' in row[:b]) or b == -1:
        ans.append(0)
    
    return ans
    
def compress_row(l):
    ans = []
    for lis in l:
        #temp = []
        for k in lis[1]:
            if [lis[0], k] not in ans:
                ans.append([lis[0],k])
            #ans.append(temp)    
    return ans

def compress_col(l):
    ans = []
    for lis in l:
        for k in lis[0]:
            if [k, lis[1]] not in ans:
                ans.append([k, lis[1]])
    return ans

def compress(l):
    ans = []
    for lis in l:
        if lis not in ans:
            ans.append(lis)
    return ans 
# print(rows(cross)) 
cross = cross.strip('\n')      
row = rows(cross)
cols = get_columns(rows(cross))
# print(row)
# print()
# print(cols)

ans = []
roww = []
col = []      
for ind,r in enumerate(row):
    # print(r)
    if block(r) != []:
        # print(block(r))
        roww.append([ind,block(r)])

    
# print("iotuiiurwe")

# print(cols[1].index(BLACK))
for ind,c in enumerate(cols):
    # print(c, block(c))
    if block(c) != []:
        col.append([block(c), ind])

# print( col)
# print(ans)
# print((roww))

# print(compress_row(roww))
# print(col)
# print(compress_col(col))
ans = compress_col(col) + compress_row(roww)
# print(sorted(ans))
# l = tuple(sorted((compress(ans))))
print([num for num in enumerate(compress(sorted(ans)), start = 1)])

# l.sort()
# print(l)
# l = list(set(tuple(i) for i in l))
# print(sorted(l))
# print((a, b, ind) for ind, a,b in [enumerate(tuple((sorted(l))))])
# print(compress(ans))
